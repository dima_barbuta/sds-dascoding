<?php

namespace App\Http\Controllers;

use App\Models\Response\BaseError;
use App\Models\Response\Error;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index(){
        return 'test';
    }

    /**
     * @param $response
     * @return false|string
     */
    public function sendResponse($response){
        return json_encode($response);
    }

    /**
     * @param Validator $validator
     * @return false|string
     */
    public function validatingError(Validator $validator)
    {
        $baseError = new BaseError();
        $messages = $validator->getMessageBag();
        foreach($messages->getMessages() as $ValidatorMessages){
            foreach($ValidatorMessages as $message){
                $error = new Error();
                $error->setMessage($message);
                $baseError->addToErrors($error);
            }
        }
        return json_encode($baseError);
    }

    /**
     * @param $message
     * @param null $code
     * @return false|string
     */
    public function throwError($message, $code = null){
        $baseError = new BaseError();
        $error = new Error();
        $error->setMessage($message);
        $error->setCode($code);
        $baseError->addToErrors($error);
        return json_encode($baseError);
    }
}
