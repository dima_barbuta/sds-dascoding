<?php

namespace App\Http\Controllers;

use App\Enums\RolesEnums;
use App\Models\Company;
use App\Models\Response\SuccessResponse;
use App\Models\Role;
use App\Models\User;
use App\Models\UserRoles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Laravel\Passport\RefreshTokenRepository;
use Laravel\Passport\TokenRepository;
use function Symfony\Component\String\s;

class PassportController extends Controller
{
    /**
     * @param Request $request
     * @return false|\Illuminate\Http\JsonResponse|string
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            'company_name' => 'required'
        ]);

        if($validator->fails()){
            return $this->validatingError($validator);
        }
        try{

            DB::beginTransaction();

            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
            ]);

            if($user){
                $company = Company::create([
                    'name' => $request->company_name,
                    'user_id' => $user->id
                ]);

                if(!$company){
                    DB::rollback();
                    $this->throwError('Can\'t create company');
                }

                $role = Role::where('slug', RolesEnums::ROLE_ADMIN)->first();


                if($role){
                    $userRoles = UserRoles::create([
                        'user_id' => $user->id,
                        'role_id' => $role->id
                    ]);

                    if(!$userRoles){
                        $this->throwError('Can\'t create role');
                    }
                }

                DB::commit();
            }else{
                DB::rollback();
                $this->throwError('Can\'t register');
            }

            $token = $user->createToken('TutsForWeb')->accessToken;
        }catch(\Throwable $e){
           return $this->throwError($e->getMessage() . ', file:' . $e->getFile() . ', line:' . $e->getLine());
        }

        return response()->json(['token' => $token], 200);
    }

    /**
     * @param Request $request
     * @return false|\Illuminate\Http\JsonResponse|string
     */
    public function login(Request $request)
    {
        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|min:6',
        ]);

        if($validator->fails()){
            return $this->validatingError($validator);
        }

        if(auth()->attempt($credentials)){
            $token = auth()->user()->createToken('TutsForWeb')->accessToken;
            return response()->json(['token' => $token], 200);
        }else{
            return response()->json(['error' => 'UnAuthorised'], 401);
        }
    }

    public function logoutApi()
    {
        $user = auth()->user();
        if($user){
            $user->token()->revoke();

            $response = new SuccessResponse();
            $response->setMessage('Logged out');
            return $this->sendResponse($response);
        }
        return false;

    }

    /**
     * @return false|string
     */
    public function details()
    {
        if(auth()->user()){
            $user = User::with('company')->find(auth()->user()->getAuthIdentifier());
            $response = new SuccessResponse();
            $response->setData($user);
            return $this->sendResponse($response);
        }else{
            return $this->throwError('Unauthorized', 401);
        }
    }
}
