<?php

namespace App\Http\Controllers;


use App\Enums\Errors;
use App\Enums\PermissionEnums;
use App\Models\Department;
use App\Models\Response\SuccessResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class DepartmentController extends Controller
{
    /**
     * @return Response
     */
    public function index()
    {

    }

    /**
     * @return Response
     */
    public function create()
    {

    }

    /**
     * @param Request $request
     * @return false|string
     */
    public function store(Request $request)
    {
        if(!$request->user()->can(PermissionEnums::PERMISSION_CREATE)){
            return $this->throwError(Errors::FORBIDDEN_MESSAGE, Errors::FORBIDDEN_CODE);
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if($validator->fails()){
            return $this->validatingError($validator);
        }

        try{
            if(!$request->user()->company){
                return $this->throwError(Errors::NO_COMPANY_MESSAGE);
            }

            $department = Department::create([
                'name' => $request->name,
                'company_id' => $request->user()->company->id
            ]);

            if($department){
                $response = new SuccessResponse();
                $response->setData($department);

                return $this->sendResponse($response);
            }else{
                return $this->throwError('Can\'t create Department');
            }
        }catch(\Throwable $e){
            return $this->throwError($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param $id
     * @return Department|string
     */
    public function show($id)
    {
        try{
            $user = auth()->user();

            if(!$user->can(PermissionEnums::PERMISSION_VIEW)){
                return $this->throwError('Access forbidden', 403);
            }

            if(!$user->company){
                return $this->throwError(Errors::NO_COMPANY_MESSAGE);
            }

            $department = Department::where('company_id', $user->company->id)->find($id);

            if($department){
                $response = new SuccessResponse();
                $response->setData($department);

                return $this->sendResponse($response);
            }else{
                return $this->throwError('Department not found');
            }
        }catch(\Throwable $e){
            return $this->throwError($e->getMessage(), $e->getCode());
        }
    }

    /**
     *
     */
    public function edit()
    {

    }

    /**
     * @param Request $request
     * @param $id
     * @return false|string
     */
    public function update(Request $request, $id)
    {
        if(!$request->user()->can(PermissionEnums::PERMISSION_EDIT)){
            return $this->throwError('Access forbidden', 403);
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if($validator->fails()){
            return $this->validatingError($validator);
        }

        try{
            $user = auth()->user();

            if(!$user->company){
                return $this->throwError(Errors::NO_COMPANY_MESSAGE);
            }

            $department = Department::where('company_id', $user->company->id)->find($id);

            if($department){
                $updatedDepartment = $department->update($request->all());
                if($updatedDepartment){

                    $response = new SuccessResponse();
                    $response->setMessage('Department was updated');
                    $response->setData($department);


                    return $this->sendResponse($response);
                }else{
                    return $this->throwError('Department can\'t  update');
                }
            }else{
                return $this->throwError('Department not found');
            }
        }catch(\Throwable $e){
            return $this->throwError($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param $id
     * @return false|string
     */
    public function destroy($id)
    {
        try{
            $user = auth()->user();

            if(!$user->can(PermissionEnums::PERMISSION_DELETE)){
                return $this->throwError('Access forbidden', 403);
            }

            if(!$user->company){
                return $this->throwError(Errors::NO_COMPANY_MESSAGE);
            }

            $department = Department::where('company_id', $user->company->id)->find($id);
            if($department){
                $isDelete = $department->delete();
                if($isDelete){
                    $response = new SuccessResponse();
                    $response->setMessage('Department was removed');
                    $response->setData($department);
                    return $this->sendResponse($response);
                }else{
                    return $this->throwError('Department can\'t  remove');
                }
            }else{
                return $this->throwError('Department not found');
            }
        }catch(\Throwable $e){
            return $this->throwError($e->getMessage(), $e->getCode());
        }
    }
}
