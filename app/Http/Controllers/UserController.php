<?php


namespace App\Http\Controllers;


use App\Models\Department;
use Illuminate\Http\Request;
class UserController extends Controller
{
    /**
     * @return false|string
     */
    public function departments()
    {
        $user = auth()->user();
        if(!$user->company->id){
            return $this->throwError('User no have company');
        }
        $departments = Department::where('company_id', $user->company->id)->get();

        return json_encode($departments);
    }

    /**
     * @param Request $request
     * @return false|string
     */
    public function company(Request $request)
    {
        if($request->user()->company){
            return $request->user()->company;
        }

        return $this->throwError('User no have company');
    }
}
