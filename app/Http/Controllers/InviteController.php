<?php

namespace App\Http\Controllers;

use App\Mail\InviteCreated;
use App\Models\Invite;
use App\Models\Response\SuccessResponse;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class InviteController extends Controller
{
    public function process(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required'
        ]);

        if($validator->fails()){
            return $this->validatingError($validator);
        }

        try{
            // validate the incoming request data

            do{
                $token = Str::random();
            } //check if the token already exists and if it does, try again
            while(Invite::where('token', $token)->first());

            //create a new invite record
            $invite = Invite::create([
                'email' => $request->get('email'),
                'token' => $token
            ]);

            // send the email
            $mail = Mail::to($request->get('email'))->send(new InviteCreated($invite));
            return $this->sendResponse(new SuccessResponse());
        }catch(\Throwable $e){
            return $this->throwError($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param Request $request
     * @param $token
     * @return false|string
     */
    public function accept(Request $request, $token)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required',
            'name' => 'required'
        ]);

        if($validator->fails()){
            return $this->validatingError($validator);
        }

        try{
            if(!$invite = Invite::where('token', $token)->first()){
                return $this->throwError('Invitation invalid');
            }

            $user = User::create([
                'email' => $invite->email,
                'password' => bcrypt($request->password),
                'name' => $request->name
            ]);

            if($user){
                $invite->delete();
                $successResponse = new SuccessResponse();
                $successResponse->setData($user);
                return $this->sendResponse($successResponse);

            }else{
                return $this->throwError('Can\'t register');
            }
        }catch(\Throwable $e){
            return $this->throwError($e->getMessage(), $e->getCode());
        }
    }
}
