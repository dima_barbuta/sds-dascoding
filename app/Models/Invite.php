<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Invite
 * @mixin Builder
 * @package App\Models
 */
class Invite extends Model
{
    use HasFactory;

    protected $fillable = [
        'email', 'token',
    ];
}
