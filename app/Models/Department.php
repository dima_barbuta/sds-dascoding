<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Departments
 * @mixin Builder
 * @package App\Models
 */
class Department extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'company_id'
    ];
}
