<?php


namespace App\Models\Response;


class BaseError extends BaseResponse
{
    /**
     * @var string
     */
    public $status = 'error';

    /**
     * @var Error[]
     */
    public $errors;

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return BaseError
     */
    public function setStatus(string $status): BaseError
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return Error[]
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param Error[] $errors
     * @return BaseError
     */
    public function setErrors($errors): BaseError
    {
        $this->errors = $errors;
        return $this;
    }

    /**
     * @param Error $error
     * @return BaseError
     */
    public function addToErrors($error): BaseError
    {
        $this->errors[] = $error;
        return $this;
    }
}
