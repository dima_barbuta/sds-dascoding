<?php


namespace App\Models\Response;


class SuccessResponse extends BaseResponse
{
    /**
     * @var string
     */
    public $status = 'success';

    /**
     * @var string
     */
    public $message = 'OK';

    /**
     * @var mixed
     */
    public $data;

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return SuccessResponse
     */
    public function setMessage($message): SuccessResponse
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return SuccessResponse
     */
    public function setStatus($status): SuccessResponse
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     * @return SuccessResponse
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

}
