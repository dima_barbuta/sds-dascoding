<?php


namespace App\Models\Response;


class Error
{
    /**
     * @var string
     */
    public $message;

    /**
     * @var int|string|null
     */
    public $code;

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return Error
     */
    public function setMessage($message): Error
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getCode(): ?int
    {
        return $this->code;
    }

    /**
     * @param $code
     * @return $this
     */
    public function setCode($code): Error
    {
        $this->code = $code;
        return $this;
    }

}
