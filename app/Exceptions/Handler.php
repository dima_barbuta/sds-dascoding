<?php

namespace App\Exceptions;

use App\Enums\Errors;
use App\Models\Response\BaseError;
use App\Models\Response\Error;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            $errorResponse = new BaseError();
            $error = new Error();
            $error->setMessage(Errors::UNAUTHORIZED_MESSAGE);
            $error->setCode(Errors::UNAUTHORIZED_CODE);

            $errorResponse->addToErrors($error);
            return response()
                ->json($errorResponse, 401);
        }
        die(json_encode($exception->guards()));
        $guard = array_get($exception->guards(),0);
        switch ($guard) {
            default:
                $login = 'login';
                break;
        }
        return redirect()->guest(route($login));
    }
}
