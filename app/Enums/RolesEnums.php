<?php


namespace App\Enums;


class RolesEnums extends BaseEnums
{
    const ROLE_ADMIN = 'admin';
    const ROLE_MANAGER = 'manager';
    const ROLE_VIEWER = 'viewer';
}
