<?php


namespace App\Enums;


class PermissionEnums extends BaseEnums
{
    const PERMISSION_CREATE = 'create';
    const PERMISSION_EDIT = 'edit';
    const PERMISSION_DELETE = 'delete';
    const PERMISSION_VIEW = 'view';
}
