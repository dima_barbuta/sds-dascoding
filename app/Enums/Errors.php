<?php


namespace App\Enums;


class Errors extends BaseEnums
{
    const FORBIDDEN_MESSAGE = 'Access forbidden';
    const FORBIDDEN_CODE = 403;

    const UNAUTHORIZED_MESSAGE = 'Unauthorized';
    const UNAUTHORIZED_CODE = 401;

    const NO_COMPANY_MESSAGE = 'No company found';
}
