<?php

use App\Http\Controllers\Controller;
use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\InviteController;
use App\Http\Controllers\PassportController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These``
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', [PassportController::class, 'login']);
Route::post('register', [PassportController::class, 'register']);
Route::post('accept/{token}', [InviteController::class, 'accept'])->name('accept');
Route::middleware('auth:api')->group(function(){


    Route::post('logout', [PassportController::class, 'logoutApi']);


    Route::prefix('v1')->group(function(){

        //Departments routes
        Route::prefix('departments')->group(function(){
            Route::post('create', [DepartmentController::class, 'store']);
            Route::get('{id}', [DepartmentController::class, 'show']);
            Route::put('update/{id}', [DepartmentController::class, 'update']);
            Route::delete('delete/{id}', [DepartmentController::class, 'destroy']);
        });

        Route::prefix('user')->group(function(){
            Route::post('/', [PassportController::class, 'details']);
            Route::post('company', [UserController::class, 'company']);
            Route::post('departments', [UserController::class, 'departments']);
        });

        // invitation email
        Route::post('invite', [InviteController::class, 'process'])->name('invite');
    });

});


