<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionsSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::insert([
            'name' => 'create',
            'slug' => 'create',
            'description' => 'Can create'
        ]);

        Permission::insert([
            'name' => 'edit',
            'slug' => 'edit',
            'description' => 'Can edit'
        ]);

        Permission::insert([
            'name' => 'delete',
            'slug' => 'delete',
            'description' => 'Can delete'
        ]);

        Permission::insert([
            'name' => 'view',
            'slug' => 'view',
            'description' => 'Can view'
        ]);
    }
}
