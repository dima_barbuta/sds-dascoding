<?php

namespace Database\Seeders;

use App\Models\Company;
use App\Models\Department;
use App\Models\User;
use Illuminate\Database\Seeder;

class UsersSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::insert([
            'name' => 'Dima',
            'email' => 'dima@gmail.com',
            'password' => bcrypt('123456')
        ]);

        Company::insert([
            'name' => 'Dascoding',
            'user_id' => 1
        ]);

        Department::insert([
            'name' => 'Main Department',
            'company_id' => 1
        ]);
    }
}
