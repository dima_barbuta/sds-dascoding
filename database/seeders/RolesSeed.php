<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RolesSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::insert([
            'name' => 'Admin',
            'slug' => 'admin',
            'description' => 'Is Admin'
        ]);

        Role::insert([
            'name' => 'Manager',
            'slug' => 'manager',
            'description' => 'Is Manager'
        ]);

        Role::insert([
            'name' => 'Viewer',
            'slug' => 'viewer',
            'description' => 'Is Viewer'
        ]);
    }
}
